- id: training_sessions_basics
  name:     GitLab with Git Basics Training
  sow_name: Basic Training
  desc_url: /services/education/gitlab-basics/
  short_desc: GitLab training on using GitLab for source control management, as well as an overview of basic git concepts and command-line instructions.
  long_desc: |
    This class is designed to provide users with an introduction to GitLab. It starts with an overview of GitLab so you can learn the basics about what GitLab does and why DevOps teams use it. Then it dives into Git, the version control system used by GitLab for source code management (SCM). You'll learn and practice fundamental Git concepts and commands. Throughout the course flow we'll provide demos and hands-on practice with the foundational processes and tasks that DevOps teams use in GitLab, such as committing changes, creating branches and merge requests, using a CI/CD pipeline, and accessing security scanning.
    <br/><br/>
    <strong>New certification!</strong> Starting in May 2020, we're including class participant access to our new <a href ="https://about.gitlab.com/services/education/gitlab-certified-associate/">GitLab Certified Associate</a> certification assessments. To earn certification, GitLab with Git Basics Training participants must receive a passing score on both a written assessment and a lab assessment evaluated by a GitLab Professional Services Engineer.
  description2: 
    Covers what GitLab does, why DevOps teams use it, and how it works with Git; Key processes and tasks teams work on in GitLab, such as committing changes, creating branches and merge requests, using a CI/CD pipeline, and accessing security scanning; Includes access to GitLab Certified Associate assessments
 
  target_audience: Any team member new to Git and GitLab
  learning_objectives:
    - Explain what GitLab is and why teams use it
    - Perform basic Git commands for branching, merging, and remote work
    - Apply fundamental concepts and skills using GitLab within the DevOps lifecycle
  prereqs:
    - Agile development
    - Source code management
  preresources:
    - name: Why you should move to Git
      href: https://www.youtube.com/watch?v=j16QHjv29A0
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 7000
  price_desc: Remote delivery is $5,000 per class. Onsite delivery is $7,000 per class plus incurred trainer travel expenses.
  public_price: 399 (not yet available - coming soon)
  facility_price: 2000
  unit: session
  duration: 1
  duration_desc: One delivery of this course includes two 4-hour remote sessions or one 1-day onsite session.
  sku: GL-BAS
  class_size: Maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1tZxq2Pkig_6ZDfxF_ha7iVHIuTr4NtIZvMS2o0Sh88Y/edit
  stages: Plan, Create, Verify

- id: training_sessions_basics_remote
  name:     GitLab with Git Basics Training - Remote Delivery
  sow_name: Basic Training - Remote SOW
  desc_url: /services/education/gitlab-basics/
  description2: 
    This is GL-BAS delivered remotely using video conferencing in two 3- to 4-hour sessions
  target_audience: Any team member new to Git and GitLab
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  price_desc: Remote delivery is $5,000 per class. Onsite delivery is $7,000 per class plus incurred trainer travel expenses.
  unit: session
  duration: 1
  duration_desc: One delivery of this course includes two 3-hour remote sessions or one 1-day onsite session. 
  sku: GL-BAS-R
  class_size: maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1wJ3a1NfI0SyA1KLweqT6U75oByA5oQT16XyDkMYZGr8/edit?usp=sharing
  stages: Plan, Create, Verify

- id: training_sessions_security_essentials_remote
  name:     GitLab Security Essentials
  sow_name: Security Essentials Training - Remote SOW
  desc_url: /services/education/security-essentials/
  short_desc: Live security training for Gold/Ultimate customers
  long_desc: |
    This course covers all of the essential security capabilities of GitLab, including SAST, DAST, dependency and container scanning, license compliance, WAF, and policy management. It is intended for customers with Gold or Ultimate subscription levels.
    <br/><br/>
    <strong>New certification!</strong> Starting in December 2020, we're including class participant access to our new <a href ="https://about.gitlab.com/services/education/gitlab-security-specialist/">GitLab Certified Security Specialist</a> certification assessments. To earn this certification, GitLab Security Essentials live training participants must receive a passing score on both a written assessment and a lab assessment evaluated by a GitLab Professional Services Engineer.
  description2: 
    Covers all of the essential security capabilities of GitLab, including SAST, DAST, dependency and container scanning, license compliance, WAF, and policy management. Intended for customers with Gold or Ultimate subscription levels. Includes access to GitLab Certified Security Specialist assessments
 
  target_audience: Project managers, developers, DevSecOps engineers, and security specialists
  learning_objectives:
    - Describe the security features available in GitLab
    - Determine teams and/or team members that should give merge request security approvals
    - Enable and configure scanning tool, including enabling and disabling options
    - Enable and configure merge request security approvals
    - View and utilize the Security Dashboard for a given group and project
    - Download scanning results as evidence for compliance
    - Configure defensive mechanisms
    - Test performance and inspect logs

  prereqs:
    - Familiarity with agile development and source code management
    - <a href ="https://about.gitlab.com/services/education/gitlab-basics/">GitLab with Git Basics course</a> or equivalent knowledge
  preresources:
    - name: Why you should move to Git
      href: https://www.youtube.com/watch?v=j16QHjv29A0
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 7000
  price_desc: Remote delivery is $5,000 per class. Onsite delivery is $7,000 per class plus incurred trainer travel expenses.
  facility_price: 2000
  unit: session
  duration: 1
  duration_desc: One delivery of this course includes two 4-hour remote sessions or one 1-day onsite session.
  sku: GL-SEC-R
  class_size: maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1glYSjPuys_MMiLSi9_ePYCAoKkWyZmU4BCgNZpicu7c/edit?usp=sharing
  stages: Secure, Protect

- id: training_sessions_pm
  name:     GitLab for Project Managers Training
  sow_name:     GitLab for Project Managers Training
  desc_url: /services/education/pm/
  short_desc: GitLab training on using GitLab issues, epics, and other project management tools to manage projects, products, and business processes.
  long_desc: |
    This class is designed to provide users with an introduction to GitLab's Plan stage - where users can manage software products or other projects using issues.  It focuses on the various tools available, including issues, epics, milestones, burndown charts, kanban boards, and agile portfolio management tools.
        <br/><br/>
    <strong>New certification!</strong> Starting in December 2020, we're including class participant access to our new <a href ="https://about.gitlab.com/services/education/gitlab-project-management-specialist/">GitLab Certified Project Management Specialist</a> certification assessments. To earn certification, GitLab for Project Managers Training participants must receive a passing score on both a written assessment and a lab assessment evaluated by a GitLab Professional Services Engineer.
  description2: 
    Covers how to set up projects by creating issues, labels, milestones, and groups; Approaches for managing projects using GitLab boards, epics, and roadmaps; Best practices for using GitLab to develop portfolio plans
  target_audience: Anyone who will be performing project management tasks using GitLab, including project managers, program managers, product owners, and scrum masters
  learning_objectives:
    - Set up projects by creating issues, labels, milestones, and groups
    - Manage projects using GitLab boards, epics, and roadmaps
    - Use GitLab to develop portfolio plans
  prereqs:
    - Agile development
    - Source code management
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 7000
  price_desc: Remote delivery is $5,000 per class. Onsite delivery is $7,000 per class plus incurred trainer travel expenses.
  unit: session
  duration: 1
  duration_desc: One delivery of this course includes two 4-hour remote sessions or one 1-day onsite session.
  sku: GL-PM
  class_size: maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1m7wA05YjvgH1FmwJBCE3v4xDe-FA37xU2Saj38XANy8/edit
  stages: Plan

- id: training_sessions_pm_remote
  name:     GitLab for Project Managers Training - Remote Delivery
  sow_name:     Project Management Training - Remote SOW
  desc_url: /services/education/pm/
  description2: 
    This is GL-PM delivered remotely using video conferencing in two 3- to 4-hour sessions
  target_audience: Anyone who will be performing project management tasks using GitLab, including project managers, program managers, product owners, and scrum masters
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  price_desc: $5,000
  unit: session
  duration: 1
  duration_desc: 6 hours
  sku: GL-PM-R
  class_size: maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1hXSgaOxMrnQrWlI3LaMY-pnDERbfTRcnEx4l3kf3lc0/edit?usp=sharing
  stages: Plan

- id: training_sessions_cicd
  name:     GitLab CI/CD Training
  sow_name: GitLab CI CD Training
  desc_url: /services/education/gitlab-ci
  short_desc: GitLab training on using the Verify, Package, and Release stages to support your team's CI/CD efforts.
  long_desc: |
    This class covers setting up continuous integration/continuous deployment (CI/CD) for your projects. It starts with a round-robin discussion of where your team is at with CI/CD today. It then focuses on what is CI/CD, why it should be used, and how to implement within GitLab. This class includes live demonstrations of the use of GitLab.
    <br/><br/>
    <strong>New certification!</strong> Starting in July 2020, we're including class participant access to our new <a href ="https://about.gitlab.com/services/education/gitlab-cicd-specialist/">GitLab CI/CD Specialist</a> certification assessments. To earn certification, GitLab CI/CD Training participants must receive a passing score on both a written assessment and a lab assessment evaluated by a GitLab Professional Services Engineer
  description2: 
    Covers what CI/CD does, why DevOps teams use it, and how it works within GitLab; How to set up and apply CI/CD inside GitLab; Includes access to GitLab  CI/CD Specialist certification assessments 
  level:    1 - Product
  target_audience: Technical Project Leads
  learning_objectives:
    - Describe what CI/CD is
    - Explain how runners work
    - Set up and configure CI/CD and runners
    - Verify a new feature
    - Scope and persist variables at various levels
    - Scaffold out the basics of a test, build, review, and deploy pipeline leveraging feature/topic branching as the review mechanism
    - Release and deployment workflow
    - Artifacts and dependency caching
    - Building and deploying images to GitLab registry
  prereqs:
    - Familiarity with agile development and source code management
    - <a href ="https://about.gitlab.com/services/education/gitlab-basics/">GitLab with Git Basics course</a> or equivalent knowledge
  preresources:
    - name: Why you should move to Git
      href: https://www.youtube.com/watch?v=j16QHjv29A0
    - name: GitLab CI/CD overview
      href: https://about.gitlab.com/features/gitlab-ci-cd/
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 7000
  price_desc: Remote delivery is $5,000 per class. Onsite delivery is $7,000 per class plus incurred trainer travel expenses.
  facility_price: 2000
  unit: session
  duration: 1
  duration_desc: One delivery of this course includes two 4-hour remote sessions or one 1-day onsite session.
  sku: GL-CICD
  class_size: maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1ncYAJu2Snq2DYvA1Ty0VZNhPKqXWJi-8_tOtA16uSZU/edit
  stages: Verify, Package, Release

- id: training_sessions_cicd_remote
  name:     GitLab CI/CD Training - Remote Delivery
  sow_name: CI CD Training - Remote SOW
  desc_url: /services/education/gitlab-ci
  description2: 
    This is GL-CICD delivered remotely using video conferencing in two 3- to 4-hour sessions
  target_audience: Technical Project Leads
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  price_desc: Remote delivery is $5,000 per class. Onsite delivery is $7,000 per class plus incurred trainer travel expenses.
  unit: session
  duration: 1
  duration_desc: One delivery of this course includes two 4-hour remote sessions or one 1-day onsite session.
  sku: GL-CICD-R
  class_size: maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1p9fO5y-nob05vAoIeYuqLZjJ_ncsrb59b3eCdF9jjUI/edit?usp=sharing
  stages: Verify, Package, Release


- id: training_sessions_admin
  name:     GitLab System Administration Basic Training
  sow_name: Custom
  desc_url: /services/education/admin
  short_desc: GitLab training on how to set up and update your self-managed GitLab instance.
  long_desc: |
    The course is customized for your specific deployment of GitLab. It covers basic installation, configuration and maintenance tasks for a GitLab self-managed instance. This course is not intended for gitlab.com customers.
  description2: 
    Covers basic setup and updating of a GitLab self-managed instance, customized to your specific implementation
  level:    1 - Product
  target_audience: Tier 1 system administrators who serve as the GitLab platform owners for their organization, and who are responsible for the initial set up and updating of a GitLab self-managed instance. 
  learning_objectives:
    - Install GitLab
    - Configure basic settings
    - Add and remove users and adjust settings
    - View and track user permission levels and changes made to these
    - Manage user access via integrations with external authentication and authorization providers
    - Configure and test optional settings and integrations
    - Perform backups, restores, and upgrades
  prereqs:
    - <a href ="https://about.gitlab.com/services/education/gitlab-certified-associate/">GitLab Certified Associate</a> certification or equivalent knowledge. 
  preresources:
    - name: GitLab with Git Basics Training
      href: https://about.gitlab.com/services/education/gitlab-basics/
    - name: Why you should move to Git
      href: https://www.youtube.com/watch?v=j16QHjv29A0
    - name: Git-ing Started with Git
      href: https://www.youtube.com/watch?v=Ce5nz5n41z4&list=PLFGfElNsQthbTRBiMVV2JTQfUUpIlqBEw
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: Varies based on specific customer deployment
  price_desc: Varies based on customized course topics
  facility_price: 2000
  unit: session
  duration: 1
  duration_desc: Custom based on specific implementation. Typical delivery of this course includes two 4-hour remote sessions or one 1-day onsite session.
  sku: GL-ADMBAS
  class_size: maximum 12 attendees
  standard_sow: #TODO
  stages: Manage

- id: training_sessions_adv_admin
  name:     GitLab Advanced System Administration Training
  sow_name: Custom course
  desc_url: /services/education/adv-admin
  short_desc: GitLab training on how to monitor and debug your self-managed GitLab instance.
  long_desc: |
    This course is customized for your specific deployment of GitLab. It covers advanced monitoring and troubleshooting tasks for a GitLab self-managed instance. This course is not intended for gitlab.com customers.
  description2: 
    Covers advanced monitoring and troubleshooting for a GitLab self-managed instance, customized to your specific implementation
  level:    1 - Product
  target_audience: Tier 2 system administrators or support team members for a GitLab self-managed instance responsible for monitoring and troubleshooting at their organization. 
  learning_objectives:
    - Apply best practices for using CLI Utilities
    - Interpret information found in Active Logs and status reports to monitor and troubleshoot issues
    - Resolve architecture breaks
    - Apply best practices for preventing integration problems
    - Resolve user and group problems
    - Work with GitLab Support to troubleshoot errors
  prereqs:
    - <a href ="https://about.gitlab.com/services/education/gitlab-certified-associate/">GitLab Certified Associate</a> certification or equivalent knowledge. 
    - <a href ="https://about.gitlab.com/services/education/admin/">GitLab System Administration Basic Training</a> course completion or equivalent knowledge. 
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: Varies based on specific customer deployment
  price_desc: Varies based on customized course topics
  facility_price: 2000
  unit: session
  duration: 1
  duration_desc: Custom based on specific implementation. Typical delivery of this course includes two 4-hour remote sessions or one 1-day onsite session.
  sku: GL-ADMADV
  class_size: maximum 12 attendees
  standard_sow: #TODO
  stages: Manage

- id: training_sessions_specialized
  name:     GitLab Specialized Training
  sow_name:     Specialized Training
  desc_url: /services/specialized/
  short_desc: GitLab training customized to your specific needs.
  long_desc: |
    This course is great for organizations looking to customize the training they receive specific to their needs.
  description2: GitLab training customized to your specific needs
  target_audience: varies by scope
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: varies
  price_desc: varies by scope of learning needs
  unit: session
  duration: 1
  duration_desc: varies by scope of learning needs
  sku: N/A
  class_size: maximum 12 attendees
  standard_sow: #TODO
  stages: varies by scope

- id: training_sessions_devops
  name:     GitLab DevOps Fundamentals Training
  sow_name:     DevOps Fundamentals Training
  desc_url: /services/education/devops-fundamentals
  long_desc: |
    Concurrent DevOps is a new way of thinking about how we create and ship software. Rather than organizing work in a sequence of steps and handoffs, the power of working concurrently is in unleashing collaboration across the organization. Throughout the four courses comprising this learning offering (GitLab with Git Basics, CI/CD, Project Management, and Security Essentials), we’ll walk a cohort of your team members through the fundamentals of using GitLab as a complete DevOps platform. Attendees will gain hands-on experience using GitLab throughout the lifecycle stages of Manage, Plan, Create, Verify, Package, Secure, Release, Configure, Monitor, and Protect.
    <br/><br/>
    <strong>Note</strong>: Due to the comprehensiveness of the GitLab capabilities covered in this course, it is recommended for customers with Gold/Ultimate licenses. 
        <br/><br/>
    <strong>New certification!</strong> Starting in December 2020, we're giving course participants access to our new <a href ="https://about.gitlab.com/services/education/gitlab-certified-devops-pro/">GitLab Certified DevOps Professional</a> certification assessments. To earn certification, GitLab DevOps Fundamentals Training participants must receive a passing score on a series of written and lab assessments evaluated by a GitLab Professional Services Engineer.
  description2: 
    Covers all key features and capabilities for each stage in the GitLab DevOps lifecycle through four courses
  level:    1 - Product
  category: Education
  target_audience: |
    All DevOps team members with GitLab Gold/Ultimate licenses, including:
      <ul>
        <li>Technical leads</li>
        <li>Project managers</li>
        <li>Developers</li>
        <li>Engineers</li>
        <li>Security specialists</li>
      </ul>
  learning_objectives:
    - Explain what Git and GitLab are and how they work
    - Describe how GitLab facilitates a concurrent devops lifecycle
    - Use GitLab throughout each stage of the concurrent DevOps lifecycle
  prereqs:
    - Agile development
    - YAML
    - Markdown
    - Code creation tools used at your organization
  preresources:
    - name: GitLab Demo
      href: https://www.youtube.com/watch?v=XbE8IXlfTJg
    - name: GitLab Complete DevOps
      href: https://www.youtube.com/watch?v=68rGlAihKFw
  maturity: Viable
  planned-maturity: Lovable
  price: 20000
  price_desc: Remote delivery is $20,000. Onsite delivery is $20,000 plus incurred trainer travel expenses. Delivery include coordination of the cohort's experience of the presentations, demos, and hands-on labs across all four courses for optimal continuity.
  unit: session
  facility_price: 8000
  class_size: maximum 12 attendees per cohort
  duration: 4
  duration_desc: One delivery of this course includes eight 4-hour remote sessions or four 1-day onsite sessions.
  sku: GL-DOF
  standard_sow: https://docs.google.com/document/d/15tI6A8nU9pYIn8nPyeO7FTW-PJXSDfYCrlCgMJwzMYI/edit?usp=sharing
  stages: All

- id: integration_jira
  name:     JIRA Integration
  desc_url: /services/integration
  level:    1 - Product
  category: Integration
  maturity: Viable
  planned-maturity: Lovable
  price: 2400
  duration: 1
  duration_desc: ~1-3 days
  sku: GL-JIRA
  standard_sow: https://docs.google.com/document/d/1QwlpyetVQ_J27Vp8quZsayu7xgJxk5wdcnmNK7OvAiQ/edit

- id: integration_jenkins
  name:     Jenkins Integration
  desc_url: /services/integration
  level:    1 - Product
  category: Integration
  maturity: Viable
  planned-maturity: Lovable
  price: 2400
  price_desc: $2,400
  duration: 1
  duration_desc: ~1-3 days
  sku: GL-JENKINS
  standard_sow: https://docs.google.com/document/d/1sQWzVVpF4U9mKYgZ2VEPcMDznjQ0qlO8O5ZuTwAMCZ0/edit

- id: integration_ldap
  name:     LDAP Integration
  desc_url: /services/integration
  level:    1 - Product
  category: Integration
  maturity: Viable
  planned-maturity: Lovable
  price: 2400
  price_desc: $2,400
  duration: 1
  duration_desc: ~1-3 days
  sku: GL-LDAP
  standard_sow: https://docs.google.com/document/d/1pIgzOv4p1MAxfIn6n3jYJ_0LsupDnNPFDCg0WebG0VQ/edit#

#- id: migration_jenkins
#  name: Jenkins Migration
#  desc_url: /services/migration/jenkins
#  level: 1 - Product
#  category: Migration
#  maturity: Minimal
#  planned-maturity: Lovable
#  price: TBD
#  price_desc: TBD
#  duration: TBD

- id: training_sessions_is
  name:     GitLab InnerSourcing Training
  sow_name:     GitLab InnerSourcing Training
  desc_url: /services/education/innersourcing-course/
  short_desc: GitLab training  on benefits and best practices for adopting an InnerSourcing approach across development teams.
  long_desc: |
    This hands-on workshop covers what InnerSourcing is, the key components needed for InnerSourcing to be successful, and how it will benefit you and your company. Attendees will learn how GitLab can help drive collaboration and consistency throughout the organization, and begin to apply best practices used by GitLab's own teams.
        <br/><br/>
    <strong>New certification!</strong> Starting in June 2020, we're including class participant access to our new <a href ="https://about.gitlab.com/services/education/gitlab-innersourcing-specialist/">GitLab Innersourcing Specialist</a> certification assessments. To earn certification, GitLab InnerSourcing Training participants must receive a passing score on both a written assessment and a lab assessment evaluated by a GitLab Professional Services Engineer.
  description2: 
    Covers what InnerSourcing is, why development teams use it, and the key components for success; Best practices used by GitLab's own development teams; Includes access to GitLab InnerSourcing Specialist certification assessments
  target_audience: Cross-functional development team members
  learning_objectives:
    - Define What InnerSourcing is
    - Identify the key components needed for InnerSourcing to be successful
    - Assess how it will benefit you and your company
    - Explain GitLabs’s CI/CD Functions
    - Apply GitLab's Permission Model
    - Describe GitLabs’s Package and Release Stages
    - Apply InnerSourcing Best Practices
    - Apply InnerSourcing functions within GitLab
  prereqs:
    - Agile development
    - Source code management
  level:    1 - Product
  category: Education
  stages: All
  maturity: Viable
  planned-maturity: Lovable
  price: 7000
  price_desc: $7,000, plus additional instructor T&E
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-IS
  class_size: maximum 12 attendees
  standard_sow: https://docs.google.com/document/d/1snRZB0VYZE8dvr5Vl2ihOySWeDCJDr8kxdGtTghiGVg/edit?usp=sharing

- id: training_sessions_is_remote
  name:     GitLab InnerSourcing Training - Remote Delivery
  sow_name:     GitLab InnerSourcing Training - Remote
  desc_url: /services/education/innersourcing-course/
  short_desc: GitLab training  on benefits and best practices for adopting an InnerSourcing approach across development teams.
  long_desc: |
    This hands-on workshop covers what InnerSourcing is, the key components needed for InnerSourcing to be successful, and how it will benefit you and your company. Attendees will learn how GitLab can help drive collaboration and consistency throughout the organization, and begin to apply best practices used by GitLab's own teams.
  description2: 
    This is GL-IS delivered remotely using video conferencing in two 3- to 4-hour sessions
  target_audience: Cross-functional development team members
  learning_objectives:
    - Define What InnerSourcing is
    - Identify the key components needed for InnerSourcing to be successful
    - Assess how it will benefit you and your company
    - Explain GitLabs’s CI/CD Functions
    - Apply GitLab's Permission Model
    - Describe GitLabs’s Package and Release Stages
    - Apply InnerSourcing Best Practices
    - Apply InnerSourcing functions within GitLab
  prereqs:
    - Agile development
    - Source code management
  level:    1 - Product
  category: Education
  stages: All
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  price_desc: $5,000
  unit: session
  duration: 1
  duration_desc: 6 hours
  sku: GL-IS-R
  class_size: maximum 12 attendees 
  standard_sow: https://docs.google.com/document/d/1-EcMLybE-gGBgNBbmxc2hrMdF8UbhGKYlbDcrfTM_dE/edit?usp=sharing

- id: implementation-rapid-results-sm
  name: Rapid Results - Self Managed
  desc_url: /services/rapid-results/
  short_desc: Rapid results helps you quickly get users to be productive using gitlab.
  long_desc: The GitLab Rapid Results Consulting package helps you quickly implement your GitLab solution (install, configure for disaster recovery) and enable your organization to take advantage of your GitLab purchase quickly. Our Professional Services Engineers aid in installing and configuring your GitLab solution so that it runs as smoothly as possible. The Professional Services Engineer will be dedicated to working with your team to architect and advise on best practices for getting the most value possible from your GitLab purchase.
  level:    1 - Product
  category: Implementation
  maturity: Viable
  planned-maturity: Lovable
  price: 25000
  duration: 15
  duration_desc: 15 days
  sku: GL-RR-SELF
  standard_sow: https://docs.google.com/document/d/1a60Nmqc1s2EfzD1nlx-peyoiSjA6kUuW_KTs3i24D2Y/edit?usp=sharing

- id: implementation-rapid-results-com
  name: Rapid Results - Gitlab.com
  desc_url: /services/rapid-results/
  short_desc: Rapid results helps you quickly get users to be productive using gitlab.
  long_desc: The GitLab Rapid Results Consulting package helps you quickly implement your GitLab solution and enable your organization to take advantage of your GitLab purchase quickly. Our Professional Services Engineers aid in installing and configuring your GitLab solution so that it runs as smoothly as possible. The Professional Services Engineer will be dedicated to working with your team to architect and advise on best practices for getting the most value possible from your GitLab purchase.
  level:    1 - Product
  category: Implementation
  maturity: Viable
  planned-maturity: Lovable
  price: 17500
  duration: 10
  duration_desc: 10 days
  sku: GL-RR-COM
  standard_sow: https://docs.google.com/document/d/1nP5iDNoi3iHTxOlpCkd-dL3w1Hg3-IQdB9z76ZqTTlA/edit

- id: migration-plus
  name: Migration+
  desc_url: /services/migration/migration-plus/
  short_desc: Migration+ packages our most popular migration and education services together. 
  long_desc: Migraiton+ provides source code migration from a single source system and adds 3 standard education services to help users and administrators to level up quickly on the gitlab application and workflows. 
  level:    1 - Product
  category: Migration
  maturity: Viable
  planned-maturity: Lovable
  price: 35000
  duration: 15
  duration_desc: 15 days
  sku: MIG+
  standard_sow: https://docs.google.com/document/d/1PXTz1Lvp1H9mrxyQNL_-bcDBrNWmsGQa47pMYPTXnTI/edit?usp=sharing

- id: health-check
  name: Health Check
  subtitle: Validate your self-managed GitLab deployment architecture
  short_desc: Let's evaluate your deployment architecture to provide recommendations on how to optimize for performance, stability, and availability. 
  long_desc: health check long description
  desc_url: /services/implementation/health-check
  summary: Are you planning for additional capacity or system load? Are you starting to experience less than optimal performance? The GitLab professional services team can help. We will leverage the <a href="/blog/2020/02/18/how-were-building-up-performance-testing-of-gitlab">GitLab Performance Tool</a> to determine opportunities for improvement in your current deployment. We will provide a report recommending updates to the deployment architecture to achieve the goals you've established. 
  whos-this-for: Customers who are planning for additional capacity or system load in the near future? Customers who arestarting to experience less than optimal performance in their GitLab self managed deployments. 
  whats-included0: A discovery session to ensure we're focused on solving the problems you care about
  whats-included1: An analysis of your current deployment architecture using Gitlab Performance Tool
  whats-included2: A report outlining architectural findings and recommendations to prepare to meet your goals
  level: 1 - Product
  maturity: Viable
  category: Implementation
  delivery_kit_report_example: https://docs.google.com/document/d/1j4Jmz_SCJEeeQT4uCIHiw6ngwsZyW_aAMVvSIyO2ndc/edit
  delivery_kit_kickoff_example: https://docs.google.com/presentation/d/16u41v3wBEJ1l52q9DOzx8H7fvK0-jb_E8SCbg1frydM/edit
  delivery_kit_kickoff_template: https://docs.google.com/presentation/d/1HtVIE64N94Rcc774ujllClGmYZ5y1_ApE4-O3pazR6k/edit
