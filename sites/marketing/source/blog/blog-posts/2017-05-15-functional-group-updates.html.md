---
title: "GitLab's Functional Group Updates - May 9th - May 12th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
description: "The Functional Groups at GitLab give an update on what they've been working on from May 9th - May 12th"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](https://docs.google.com/presentation/d/1kzUNKcLa2X5-Jze8y7Z8Jv_8NKOhBCPeIv1QwbWQwbI/edit#slide=id.g153a2ed090_0_57)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/AbGMo7Tckuk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://smcgivern.gitlab.io/discussion-updates/2017-05-10/#1)

Due to team members being on vacation, this Functional Group Update has only slides. Tune back next time!

<!-- end of the FG block -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1ZE9BNAnIPDKxgSyQU5subG2UfbraAn1dJy0emwdB2Sw/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/k8-Jto8FTmA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Engineering Team

[Presentation slides](https://docs.google.com/presentation/d/1LHj1ghSCM5GygLcwQWdtxK__Fe0ehyOQsBiBpYsIubc/edit#slide=id.g153a2ed090_0_63)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NlXPGkaSJR8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
