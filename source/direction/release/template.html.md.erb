---
layout: markdown_page
title: "Product Direction - Release"
description: "The Release stage comes after you've configured your production infrastructure and deployed your application to it. Learn more here!"
canonical_path: "/direction/release/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for Release. If you'd like to discuss this direction
directly with the product managers for Release, feel free to reach out to Orit Golowinski (PM of the [Release Stage](/handbook/engineering/development/ops/release/)) ([GitLab](https://gitlab.com/ogolowinski), [Email](mailto:ogolowinski@gitlab.com) [Zoom call](https://calendly.com/ogolowinski/) or Kevin Chu (Group PM) ([GitLab](https://gitlab.com/kbychu), [Email](mailto:kchu@gitlab.com) [Zoom call](https://calendly.com/kchu-gitlab/30min)).

## Overview

The Release stage puts what you've developed into the hands of customers. It comes after you've configured your continuous integration pipelines, configured your infrastructure, and before operating and monitoring for feedback and improvement. 

The Release stage helps you to continuously deploy your application to environments confidently with the granular control needed, no matter the complexity.

<%= devops_diagram(["Verify","Configure","Package","Monitor","Release"]) %>

<!-- source: https://docs.google.com/presentation/d/1606LcicYP-QKMjHHzTEvZSrx5qkW9LpHiKQUWlggdEk/edit#slide=id.g82767e51d3_1_0 -->

## Mission
Empower all teams to effectively deploy when, where, and how they want, without compromising security and support traceability naturally.

## Landscape

## Principles

## Vision

Check out our Release Stage Vision:
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/pzGCishRoh4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Release 3-year vision process 

We are working toward creating [3-year vision mock-ups](https://gitlab.com/groups/gitlab-org/-/epics/3825) to effectively orchestrate features across the Release stage.

### Release Annual and quaterly Goals 

The Release stage's main focus for this year is around [Continuous Delivery](https://about.gitlab.com/direction/release/continuous_delivery/) and specifically around supporting [DORA4 Metrics](https://gitlab.com/groups/gitlab-org/-/epics/4358) in GitLab.

- [FY22 Release Stage roadmap](https://gitlab.com/gitlab-com/Product/-/issues/1906)
  - [FY22 Q1 Release Stage goals](https://gitlab.com/groups/gitlab-org/-/epics/4696)
  - [FY22 Q2 Release Stage goals](https://gitlab.com/groups/gitlab-org/-/epics/5167)

### Personas

1. 🟩 [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. 🟩 [Allison - Application Ops](/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
1. 🟩 [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. 🟩 [Delaney - Devleopment Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. 🟩 [Rachel - Release Manager](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)

### Strategy

The three year target for Release (high level [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=✓&state=opened&label_name[]=devops%3A%3Arelease); [SMAU (Stage Monthly Active Users) dashboard](https://app.periscopedata.com/app/gitlab/661492/WIP-Ops:-Release:-Release-Management)) is:

- Deliver on the promise of [Progressive Delivery](/direction/ops/#progressive-delivery) by offering a market-leading solution.
   - [Automatically roll back](https://gitlab.com/groups/gitlab-org/-/epics/3088)
   - [Advanced deployments](https://gitlab.com/groups/gitlab-org/-/epics/2213)
   - [Feature Flags at Enterprise Grade](https://gitlab.com/groups/gitlab-org/-/epics/3976)
   - [Comprehensive and contextual dashboard for Feature Flags](https://gitlab.com/groups/gitlab-org/-/epics/2236)
   - [A/B testing based on Feature Flags](https://gitlab.com/groups/gitlab-org/-/epics/2966)
   - [Advanced Deployments for AWS](https://gitlab.com/groups/gitlab-org/-/epics/3798)
- Point GitLab to your code and your environment (whether that's Kubernetes, Azure, Google Cloud Platform, AWS, mobile marketplaces, or other app stores) and we'll automatically connect the dots to handle your deployments.
   - [Streamline Deployment to AWS](https://gitlab.com/groups/gitlab-org/-/epics/2351)
   - [Release Runbooks](https://gitlab.com/groups/gitlab-org/-/epics/2662)
   - [HashiCorp Vault Integration](https://gitlab.com/groups/gitlab-org/-/epics/2868)
- Seamlessly navigate cross-repository, multi-cloud, complex releases in ways natural to both mature organizations and new users, wherever they are at in their DevOps Journey. While automatically capturing information around changes to production for an audit or compliance need. 
   - [Advanced Deploy Freezes](https://gitlab.com/groups/gitlab-org/-/epics/2884)
   - [Compliance in Release](https://gitlab.com/groups/gitlab-org/-/epics/2379)
   - [Release Evidence](https://gitlab.com/groups/gitlab-org/-/epics/1856)
   - [Feature Flag Permissions](https://gitlab.com/gitlab-org/gitlab/-/issues/8239)
 
There are two main flows that illustrate the future vision of the Release stage. We have two embedded widgets from [Figma](https://www.figma.com/design/). To interact with these widgets, you can use the zoom or full screen icons to view the various screens and steps in the prototypes. You may notice color hints, which are prompts to click various buttons on the screens. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/UP7Ao4-vibc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Check out the two release flows below: 

**Deployment templates and automation**

<figure class="video_container">
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2Fkz8OHCblMt7QGo5a5JX1Vl%2FRelease-3-year-vision%3Fnode-id%3D330%253A5588%26scaling%3Dcontain" allowfullscreen></iframe>
</figure>

**Deployment approvals from a notification triggered by a Runbook**

<figure class="video_container">
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2Fkz8OHCblMt7QGo5a5JX1Vl%2FRelease-3-year-vision%3Fnode-id%3D330%253A16%26scaling%3Dmin-zoom" allowfullscreen></iframe>
</figure>

### Pricing

<%= partial("direction/ops/tiering/release") %>


