---
layout: markdown_page
title: Product Direction - Growth
canonical_path: "/direction/growth/"
---

## Growth Section Overview

The Growth section at GitLab was formed as of August 2019 and we are iterating along the way. We are excited by the unique opportunity to apply proven growth approaches and frameworks to a highly technical and popular open core product. Our goal is to accelerate and maximize GitLab user and revenue growth, and while doing that, to become a leading B2B product growth team and share our learnings internally and externally.

Currently, our Growth section consists of 4 product groups (adoption, activation, conversion, expansion) and 1 [product analysis](https://about.gitlab.com/direction/product-analysis/) group. Each product group consists of a cross-functional team of Growth Product Managers, Developers, and UX/Designers, with shared analytics, QA and user research functions, and the product analysis group is focused on empowering the product and growth team with analysis and insights.

### Growth Section's Principles

In essence, we are a cross-functional team of product managers, engineers, designers and data analysts with a unique focus and approach to help GitLab grow. Since GitLab Growth Section is still relatively new, we'd like to share the principles we strive to operate under, which will act as our team vision, and also help the rest of the company understand the best ways to collaborate with us.   

#### Principle 1: The Growth section focuses on connecting our users with our product value

While most traditional product teams focus on creating value for users via shipping useful product features, the Growth section focuses on connecting users with that value.  We do so by: 
* Driving feature adoption by removing barriers and providing guidance
* Lowering Customer Acquisition Cost (CAC) by maximizing the conversion rate across the user journey
* Increasing Life time value (LTV) by increasing retention & expansion
* Lowering sales/support cost by using product automation to do the work

#### Principle 2: The Growth section sits in the intersection of Sales, Marketing, Customer success & Product

Growth teams are by nature cross-functional as they interact and collaborate closely with many other functional teams, such as Product, Sales, Marketing, Customer success etc. Much of growth team's work helps to complement and maximize these team's work, and ultimately allow customers to get value from our product. 

![growth team helps](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/ece3ead382dbd91201f8e09246ecdc5c6c415643/source/images/growth/growthhelps.png)


To provide some examples:
* While the Marketing Team works on creating awareness and generating demand via channels such as webinars, content marketing & paid ads, the Growth team can help lower friction in signup and trial flows to maximize the conversion rate, thus generating higher ROI on marketing spend
* While the Sales Team works on converting larger customers in a high touch manner,the Growth team can help create self-serve flows and funnels to convert SMB customers, as well as building automated tools to make the sales process more efficient
* While the Core product Team works on building features to solve customer problems and pain points, the Growth team can help drive product engagement, create usage habit and promote multi-feature adoption
* While the Customer Success & Support Teams work on solving customers problems and driving product adoption via human interaction, the Growth Team can help build new user onboarding flows and on-context support to serve some of these needs programmatically at large scale

#### Principle 3: The Growth Section uses a data-driven and systematic approach to drive growth

Growth teams use a highly data & experiment driven approach
* We start from clearly defining what success is and how to measure it, by picking a [North Star Metric](https://about.gitlab.com/handbook/product/metrics/#north-star-metric) - the one metric that matters most to the entire business now
* Then we break down the NSM into sub-metrics, this process is called building a growth model. Growth model helps us identify key variables that contribute to GitLab's north star metric, and describes the connections between them at a high level. 
  We continuously iterate our thinking about GitLab's growth model as we learn more about our business, customers, and product, but our latest thinking is captured here [GitLab Growth Model](https://docs.google.com/presentation/d/1aGj2xCh6VhkR1DU07Nm3oaSzqDLKf2i64vlJKjn05RM/edit?usp=sharing). 


```mermaid

graph LR
	A[ARR] --> B[ARR from New Paying Customers]
	A --> C[ARR from Existing Paying Customers]
	B --> D[New customers directly sign up for Paid Plan]
	D --> E[# of Visitors]
	D --> F[Conversion Rate]
	D --> G[Average Contract Value]
	B --> H[New Trial Customers sign up for Paid Plan ARR]
	H --> I[# of Trials]
	H --> J[Conversion Rate]
	H --> K[Average Contract Value]
	B --> L[New/Existing Free Users sign up for Paid Plan ARR]
	L --> M[# of Free User Base]
	L --> N[Conversion Rate]
	L --> O[Average Contract Value]
	C --> P[Existing Paid Customer ARR]
	C --> R[Existing Customer Churned ARR]
	C --> Q[Existing Customer Expansion ARR]
	P --> S[Renewal]
	Q --> T[Seats]
	Q --> U[Upgrades]
	T --> V[# of Seats]
	T --> W[$ per Seat]
	U --> X[# of Upgrades]
	U --> Y[Delta ARR $]
	R --> Z[Cancel]
	R --> AA[Downgrade]
	AA --> AB[# of Downgrades]
	AA --> AC[Delta ARR $]
	subgraph Retention
	S
	Z
	end
	subgraph Expansion
	T
	U
	V
	W
	X
	Y
	AA
	AB
	AC
	end
	subgraph Acquisition
	E
	F
	G
	end
	subgraph Conversion
	I
	J
	K
	M
	N
	O
	end

```

Note that the terms used in growth model are broadly defined, for example, "Conversion Rate" here refers to the percentage of various non-paid customers (new, trial, free) converting to paid customers.

* Then by reviewing the sub-metrics, we try to identify the areas with highest potential for improvement, and pick focus area KPIs for those areas. We typically do so by looking for signals such as a very low conversion rate in an important funnel, metrics that seem to be low comparing with industry benchmarks, or a product flow that caused a lot of customer complaints or support tickets etc.         
* Then we use the build-measure-learn framework to test different ideas that can potentially improve the focus area  KPI. We utilize an experiment template to capture the hypothesis all the way to the experiment design, rank all experiment ideas using ICE framework, work through the experiment backlog, and then analyze the experiment results.
* Once this focus area KPI has been improved, we go back to the growth model, and identify the next areas we want to focus on 

```mermaid
graph LR
    nsm[North Star Metric]--> grm
    grm[Growth Model] --> fak
    fak[Focus Area KPI] -- Data --> ide
    ide[Ideate] --> pri
    pri[Prioritize] --> bld
    bld[Build] --> alz
    alz[Analyze] --> tst
    tst[Test] --> ide
    
    style nsm fill:#6b4fbb
    style nsm color:#ffffff
    style grm fill:#6b4fbb
    style grm color:#ffffff
    style fak fill:#fba338
    style fak color:#ffffff
    style ide fill:#fa7035
    style ide color:#ffffff
    style pri fill:#fa7035
    style pri color:#ffffff
    style bld fill:#fa7035
    style bld color:#ffffff
    style alz fill:#fa7035
    style alz color:#ffffff
    style tst fill:#fa7035
    style tst color:#ffffff
```

By following this systematic process, we try to make sure that: 1) We know what matters most; 2) Teams can work independently but their efforts will all contribute to overall growth; 3) We always aim to work on the projects with the highest ROI at any given moment. The ultimate goal is that we are not only uncovering all levers to drive growth, but also tackling them as efficiently as possible. 
#### Principle 4: The Growth Section experiments a lot and has a “Win or Learn”  mindset

***"Experiments do not fail, hypotheses are proven wrong"***

Growth teams view any changes as an “experiment”, and we try our best to measure the impact. With well-structured hypotheses and tests, we can obtain valuable insights that will guide us towards the right direction, even if some of the experiments are not successful in term of driving desired metrics. Refer to section below for a list of experiments



## Growth Section's Current Focus

### Growth Section OKR

Please refer to [here](https://about.gitlab.com/company/okrs/fy21-q4/) for latest GitLab Company OKR

### Growth KPI 
Timeframe: FY21 Q4 ~ TBD (until we believe the marginal improvements we can drive are minimal)
- Adoption Group: [New Group Namespace Create Stage Adoption Rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-create-stage-adoption-rate)
- Activation Group: [New Group Namespace Verify Stage Adoption Rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-verify-stage-adoption-rate) 
- Expansion Group: [New Group Namespace with at least two users added](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-with-at-least-two-users-added)
- Conversion Group:[New Group Namespace Trial to Paid Conversion Rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-trial-to-paid-conversion-rate) 


Please refer to [here](https://about.gitlab.com/handbook/product/performance-indicators/) for a complete set of product KPIs

## Growth Section's Current Focus Area

Starting in FY21 Q4, Growth section will take a more focused approach to work on the highest ROI growth opportunities for the business - and the first focus area we've chosen is the new customer free to paid SaaS conversion rate. 

Through analysis of our new customer acquisition funnel, we identified there is reasonable room for improvement in this area. In the past year, Growth section has launched many experiments and improvements in this area, such as new user onboarding issue board experiment, improved check-out experience etc, and we have accumulated insights and learnings to allow us to form high quality hypothesis. One key insight we've learned is that first 90 days is critical to a new customer's onboarding, and if this customer successfully adopts our key features in Create & Verify, invites team members to join, and experiences the GitLab's product value via a trial, the customer's likelihood to convert increases significantly.     

Based on this insight, we will have each of the growth groups to focus on one key actions: 
- Create Adoption: Create is the first stage most new customers need to set up, and many new customers are here for SCM. If we can improve the adoption rate of create early on, it sets up the customer for success for adopting more features and converting.
- Verify Adoption: Among all stages, using Verify actually has highest correlation with converting to paid customers, we believe this is the aha moment where new customers see the value of a complete devops platform. 
- Invite team members: GitLab is designed for teams, if we can get eligible namespaces to effectively add more team members, they are more likely to see the value of GitLab, and we believe that can drive conversion rate. 
- Trial to paid conversion: Trial is one of the most effective ways to get new customers to upgrade, and we think there is room for improvement in terms of trial to paid conversion rate. We want to improve this before driving more trials. 

This way, we can launch experiments in these 4 areas that can potentially help a new prospect customer see the value of GitLab quickly, and increase their likelihood of conversion. Because there are opportunities to drive 4 actions in some shared new customer flows and touch points, the 4 growth groups will collaborate closely to make sure we have a shared vision for the new customer journey, and won't create a situation that one customer is pulled too many different directions. We will start with our .com product, and migrate applicable learnings and improvements to our self-managed product.


## Growth Section's Mid Term Plan

In 2020, Covid-19 has brought uncertainty and disruptions to the market. In tough economic times, all businesses need to focus on efficient growth. GitLab's Growth Section aims to help the company drive growth while improving efficiency in all fronts. Therefore we've aligned our 3-Year Strategy into the 3 themes below: 

#### Theme 1: Improve customer retention to build a solid foundation for growth

Existing customer retention is our life blood, especially in tough marco-economic environments, it is critical to serve our current customers well and minimize churn. GitLab has a unique advantage with a subscription business model, and our[ gross retention](https://about.gitlab.com/handbook/product/performance-indicators/#gross-retention) is fairly strong, however if you drill down to look at [cohort-based subscription retention](https://docs.google.com/presentation/d/1RF8b0TYIgnPy1bQpwNvLYT_AIIvu2po9V4yWv9pXVAo/edit#slide=id.g83c44e3899_5_0), we still have room for improvement. On the other hand, in tough times, our customers will potentially be facing challenge and uncertainty as well, therefore it will be naturally harder to upsell them with higher tiers or more seats. By staying laser focused on retention, we can build a strong foundation to weather the storm and drive growth sustainability, and can be right there when customers are ready to expand again. 

To improve retention, the Growth Section has identified and will be working on the areas such as: 


1) Continue to make the customer purchasing flows such as renewal and billing process robust, efficient, transparent and smooth, as these are critical customer moments.  
We have done projects such as: 
* Clarifying the options between “auto-renewal” and “cancel” 
* [Providing clear information on customer licenses and usage](https://gitlab.com/gitlab-org/gitlab/-/issues/118592) 
* Automating renewal processes to minimize sales team burden

And we are also working on quarterly co-term billing for self-hosted customers, continuous improvement of CustomersDot user experience, as well as supporting critical pricing and packaging related projects. 

To make sure what we do will benefit customer, we have a weekly cross-functional sync with sales, biz ops, support, finance and closely monitor Customer Success Score (billing related) as our OKR. 

2)  Deep dive into the customer behavior and identify leading indicators for churn. This will include projects such as:  
* Creating [Customer MVC health score](https://gitlab.com/gitlab-data/analytics/-/issues/3803) and analyze usage based retention curve with help from data analytics team
* Capturing and analyzing [reasons for cancellation](https://app.periscopedata.com/app/gitlab/572742/Auto-Renew-AB-Test-Result?widget=7523649&udv=0) 
* Experimenting with different types of “early interventions” to reduce churn

3) Identify and experiment on drivers that can lead to better retention. 

Through analysis, we can identify which customer behaviors are critical to retention, and therefore we can experiment on prompting these types of behaviors among more customers. For example,  one hypothesis is that if a customer uses multiple stages and features of GitLab, they are more likely to get value from a single DevOp platform, thus they are more likely to become a long term customer. 

Therefore, in FY21, one of the Growth Section's focus areas will be on helping GitLab customers adopt more stages, and we'll observe if that leads to better retention to confirm the causation. ALong with this, we will need to conduct both [quantitative data analysis](https://gitlab.com/gitlab-data/analytics/-/issues/4762) and [quanlitative research](https://gitlab.com/gitlab-org/growth/product/-/issues/1572) to describe the baseline and understand the drivers and barriers behind stage adoption.  

```mermaid
graph LR
    Create --> Plan
    Verify --> Secure
    Create --> Manage 
    Create --> Verify
    Verify --> Release
    Verify --> Package
		Verify --> Configure
    Release --> Monitor
    Release --> Protect
```


##### Theme 2: Increase new customer conversion rate to maximize growth efficiency

In order to grow efficiently,  we also want to maximize the efficiency of new customer acquisition. For each marketing dollar we spend, we want to bring more dollars back in terms of revenue, and we also need to reduce payback time to generate cash flow that is essential for weathering storms.

As a collaborator to marketing & sales teams, the role the growth team can play here is to aggressively analyze, test and improve the new user flow and in-product experience between about.gitlab.com, GitLab.com, self-hosted instance, CustomersDot etc., with the goal of converting a prospect who doesn’t know much about GitLab, to a customer who understands the value of GitLab and happily signs up for a paid plan. 

In order to achieve this goal, we try to understand what the drivers are leading to new customer conversion and amplify them. For example, we have identified through analysis that the number of users in a team, as well the numbers of stages/features a team tries out, all seem to be correlated with a higher conversion rate to a paid plan.  Accordingly, we planed experiments and initiatives this area such as: 

1) Building a [new customer onboarding tutorial](https://gitlab.com/gitlab-org/growth/product/-/issues/107) to help users learn how to use different stages of GitLab 

2) Design better team invitation flow & triggers to increase the team size

3) Setting up [customers behavior triggers](https://gitlab.com/gitlab-org/gitlab/-/issues/215081) to enable the growth marketing team to send targeted & triggered emails 

Again, in order to drive this theme, we will also need to understand the end to end new customer journey, and identify the drivers and barriers via a series of research and analytics projects, such as: 


1) Map out GitLab new customers journey and understand any potential experience or data gaps between marketing, sales, growth, product teams 

2) Post-purchase survey & Email survey to understand the reasons behind why some customers convert, while others don’t 

3) Self-hosted new customer user research


##### Theme 3: Build out infrastructures and processes to unlock data-driven growth

Data is key to a growth team’s success. Growth team uses data & analytics daily to: define success metrics, build growth models, identify opportunities, and measure progress.  So part of our focus for FY21 will be working closely with GitLab data analytics team to build out that foundation, which include: 

1) Provide knowledge & [documentation](https://docs.google.com/presentation/d/1J0dctOP-xERo2j0WMWcaBuNqTNaaPuJTxLfIaOhNRHk/edit#slide=id.g74d58e3ba0_0_5) of currently available data sources to understand customer behaviors . Also, establish a framework and best practices to enable consistent and compliant data collection

2) Build out a Product usage metrics framework to evaluate how customers engage with certain features. For example, we successfully finished SMAU data collection and dashbaording project for all GitLab product stages in Q1, and will move on to a similar [North Star Metric project](https://gitlab.com/gitlab-org/product-intelligence/-/issues/374) for all GitLab product features in Q2. 

3) Build out Customer journey metrics framework to understand how customers flow the GitLab through funnel, including [end to end cross-functional reporting](https://gitlab.com/gitlab-data/analytics/-/issues/4336) spanning marketing, sales and growth.   


Along the way, we also aim to share our learnings and insights with the broader GitLab team and community, as we firmly believe growth is a whole company mission, and that success ultimately comes from constant learning & iteration & testing & sharing, as well as breaking the silos of functional teams to drive towards the same goal. 

## Growth Insights Knowledge Base 

### Dashboards
Below is a list of the dashboards we build and use daily. Many of these dashboards are used not only by us, but also by executives, broader product team, sales and customer success teams.   

|Area |Dashboard/Report | Description |Date|
| ------ | ------ |------ |------ |
|Overall Product| [Product Adoption Dashboard](https://app.periscopedata.com/app/gitlab/771580/Product-Adoption-Dashboard)  | Product KPIs |FY21 Q1 |
|Overall Growth| [New namespace conversion dashboard](https://app.periscopedata.com/app/gitlab/761347/Group-Namespace-Conversion-Metrics)  | Leading indicators of how new namespaces convert to paid customers |FY20 Q4 |
|Overall Product| [SMAU dashboard](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard)  | Stage Monthly Active User trend for both .com and self managed |FY20 |
|Overall Product| [Stage Retention and Adoption Dashboard](https://app.periscopedata.com/app/gitlab/621735/WIP:-SMAU-Retention-and-Adoption) | How popular are the stages among GitLab customers, and how well are they retaining customers |FY20 |
|Activation |[New Customer Acquisition Dashboard](https://app.periscopedata.com/app/gitlab/531526/Acquisition-Dashboard---Last-90-days) |Trend of acquisition of new customers |FY20 |
|Conversion |[New group namespace trial to paid conversion rate](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-trial-to-paid-conversion-rate) | How are new new groups that start trials converting to becoming paid customers |FY20 |
|Adoption |[Renewal Dashboard](https://app.periscopedata.com/app/gitlab/505939/Renewals-Dashboard) | Trend on renewal and cancellation of susbscriptions|FY20 |
|Adoption |[Retention Dashboard](https://app.periscopedata.com/app/gitlab/403244/Retention) | Retention metrics and trend |FY20 |
|Expansion | [New groups with at least 2 members in their first seven days](https://about.gitlab.com/handbook/product/performance-indicators/#new-group-namespace-with-at-least-two-users-added) | At what rate do new groups have at least 2 members and how can we increase the team adoption rate |FY20 |
|Expansion | [SPAN Deep Dive Report](https://docs.google.com/document/d/1zt3uUcPJW7Y1dMgtIsi4Z1antbPFPy8fp7Fib4Ps_qw/edit?usp=sharing) |How to understand SPAN (Stage Per Average Namespace) and ideas to improve SPAN|FY20 |
|Overall Product| [EoA Monitoring Metrics - Bronze Impact on Group Namespaces](https://app.periscopedata.com/app/gitlab/810873/EoA-Monitoring-Metrics---Bronze-Impact-on-Group-Namespaces) | How did the removal of the Bronze plan impact free-to-paid conversion as well as group namespace stage adoption |FY21 Q1 |

### Insights

|Link |Type| Description |Date |
| ------ | ------ |------ |------ |
|[Why prospects come to GitLab](https://docs.google.com/spreadsheets/d/179U8LQKeehXSiAJ1C7nMHKetmVdM5SqIeaqz43BirJI/edit?usp=sharing)|Survey|It shows top reasons propsects come to GitLab and % of win/loss by reasons |FY21 Q1 |
|[Feature Discovery Survey](https://docs.google.com/spreadsheets/d/1fAs9_5_wzZpdVs3d7D1Iqunb-scF5s_7xD9hNWHacNA/edit?usp=sharing) | Survey |Among our paid customers, which features are they aware, interested in and used? |FY21 Q1 |
|[Increase Stages per Organization](https://docs.google.com/presentation/d/1eyClYu8x2nLOhcNTcKx4-Z_IdtO6S2s9KhgImglcNCE/edit?usp=sharing)  | Analysis |How many stages does an average GitLab customer adopt and how to drive adoption |FY21 Q1|
|[What drives free to paid adoption](https://docs.google.com/presentation/d/11Ks8ArlCGzGY0VrSo68RBkbKwWD7pUEc9I9Vu0bIaR4/edit?usp=sharing) | Analysis |What are the drivers that can lead to higher free to paid conversion rate |FY20 Q4|
|[SaaS products new user onboarding research](https://docs.google.com/presentation/d/13BCqrAjY-guNdiRFXNY2ubLLmQdXUpiyaERxdEdirFA/edit#slide=id.g9dbb0f4ccd_0_0)|Research|How best-in-class SaaS products are designing their first mile and continious onboarding experience|FY21 Q1|
|[Why are prospects coming to GitLab (utilizing Command Plan info)](https://docs.google.com/spreadsheets/d/179U8LQKeehXSiAJ1C7nMHKetmVdM5SqIeaqz43BirJI/edit#gid=1745528755)| Analysis | What is the distribution of primary use cases across various sales segment and closed/won vs. closed/lost. | 2021-03-01 | 
|[Which What's New items are users most intersted in?](https://app.periscopedata.com/app/gitlab/816678/What's-New)| Dashboard | Dashboard that shows engagement with What's New items | ongoing | 
|[Why are Saas users cancelling?](https://app.periscopedata.com/app/gitlab/808007/WIP-Cancellation-Reasoning-Dashboard)| Dashboard | Dashboard that shows the self-reported reasons and verbatims that Saas customers are cancelling in CustomersDot | ongoing |  
|[Free to paid conversion rates by integration usage](https://app.periscopedata.com/app/gitlab/756008/Mike-K-Scratchpad?widget=10229400&udv=0) | Dashboard | See the correlation between integration usage and conversion from free to paid | ongoing | 







## Growth Experiments Knowledge Base 

Here is a list of our currently running and concluded experiments

| Experiment Name | Group | Status |
| ------ | ------ |------ |
|[Update .com paid sign up flow](https://gitlab.com/gitlab-org/growth/product/issues/87)|Activation|Concluded|
|[Pricing page tier presentation test](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/73)|Activation|Concluded|
|[Display security navigation to upsell new .com signups](https://gitlab.com/gitlab-org/gitlab/-/issues/34910)|Conversion|Concluded|
|[New user onboarding flow](https://gitlab.com/groups/gitlab-org/-/epics/3526)|Conversion|Concluded|
|[.com Trial flow improvements](https://gitlab.com/gitlab-org/gitlab/-/issues/119015)|Conversion|[Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/215881)|
|[MR with no pipeline Prompt](https://gitlab.com/gitlab-org/growth/product/issues/176)|Expansion|[Concluded](https://gitlab.com/gitlab-org/growth/product/-/issues/1588)|
|[Buy CI minutes option on GitLab.com top right dropdown](https://gitlab.com/groups/gitlab-org/growth/-/epics/28)|Expansion|Concluded|
|[Clarify Auto renew toggle vs. Cancel](https://gitlab.com/gitlab-org/growth/product/-/issues/43) |Adoption |[Concluded](https://gitlab.com/gitlab-org/growth/product/issues/162)|
|[MVC of What's New](https://gitlab.com/gitlab-org/growth/product/-/issues/1487) | Adoption | [Concluded](https://gitlab.com/gitlab-org/gitlab/-/issues/255349)| 
| [Required group trials](https://gitlab.com/groups/gitlab-org/-/epics/4500)  | Conversion | Concluded |
| [Optional trial during free SaaS signup](https://gitlab.com/groups/gitlab-org/-/epics/4414) | Conversion | Active |
| [SaaS trial onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4803) | Conversion | scheduled for 13.9 |
| [SaaS trial active trial status countdown](https://gitlab.com/groups/gitlab-org/-/epics/3538) | Conversion | scheduled for 13.9 |
| [SaaS continuous onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4817) | Conversion | scheduled for 13.9 |
| [Invited user email iteration 2](https://gitlab.com/gitlab-org/gitlab/-/issues/296966) | Expansion | scheduled for 13.9 |
|[Make Upload Options Accessible from Project Details Page](https://gitlab.com/groups/gitlab-org/-/epics/4742) | Adoption | Active | 
|[Check "Initialize repository with a README" by default](https://gitlab.com/groups/gitlab-org/-/epics/4962) | Adoption | Active | 
|[Add CTA for Project Integrations to Drive Deeper Adoption](https://gitlab.com/groups/gitlab-org/-/epics/4772) | Adoption | Active | 
|[Add "New Repo" CTA](https://gitlab.com/groups/gitlab-org/-/epics/4776) | Adoption | scheduled for 13.11 | 
|[Collect JTBD info in First Mile](https://gitlab.com/groups/gitlab-org/-/epics/4821) | Adoption | scheduled for 13.11 | 
|[Simplify Group Creation in First Mile](https://gitlab.com/groups/gitlab-org/-/epics/4825) | Adoption | scheduled for 13.11 | 
|[Empty versatile template for CI](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/292) | Activation | Concluded | 
|[In Product Email Campaigns](https://gitlab.com/groups/gitlab-org/growth/-/epics/62) | Activation | Active | 
|[Empty versatile template for CI for <90 days>](https://gitlab.com/gitlab-org/gitlab/-/issues/300993) | Activation | Active | 
|[Template zero state page VS Simple template](https://gitlab.com/groups/gitlab-org/growth/-/epics/61) | Activation | scheduled for 13.11 | 
|[Introduce users to verify via code quality template walkthrough](https://gitlab.com/groups/gitlab-org/growth/-/epics/68) | Activation | scheduled for 13.11 | 
|[Add Ease Score and Interview Recruitment Track to Onboarding Email Series](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/314) | Activation | scheduled for 13.11 | 




## Growth Section Areas of Focus

### 1. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [About.GitLab.com](https://about.gitlab.com/)

### 2. Growth Group Directions

* [Product Analysis Direction](https://about.gitlab.com/direction/product-analysis/)

### 3. What's next: Growth Group Issue Boards


* [Activation board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition)
* [Conversion board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
* [Expansion Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) 
* [Adoption Board](https://gitlab.com/groups/gitlab-org/-/boards/2112028?&label_name[]=group%3A%3Aadoption)
* [Product Analysis Board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=product%20analytics) 

## Growth Definitions

It can be confusing to know what we're all talking about when we use different names for the same things. This is where we can clarify and discuss common terms to make communication easier.

- *Sign-up Experience:* Registering to be a user of GitLab. It generally starts on `about.gitlab.com` and ends when the user is registered and can log in to the application. We have free, paid or trial options.
- *Trial Experience:* The experience of selecting and starting a trial, as well as testing/trialing features in GitLab.com or a self-managed instance during the trial. This could be done by net new users of GitLab, or existing users of GitLab who are interesting in exploring paid features.
- *User Onboarding:* A general term that refers to experiences we create to support users during their first 90 days, or any time they engage with a new stage.
  - *Continuous onboarding:* Specifically our onboarding experience that allows users to choose when and if they want to continue onboarding tasks. It starts with simple tasks and builds to more complex ones.
  - *First Mile:* The "happy path" flow a new user goes through when registering for GitLab and landing in the app for the first time. This is a more confined user flow, as we narrow down options so they can experience GitLab without being overwhelmed by all the other features. It starts at `about.gitlab.com` and ideally ends with them using Create features.

### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
