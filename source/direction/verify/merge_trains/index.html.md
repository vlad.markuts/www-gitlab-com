---
layout: markdown_page
title: "Category Direction - Merge Trains"
---

- TOC
{:toc}

## Merge Trains

A core tenet of our Ops section [direction](/direction/ops/#speedy-reliable-pipelines), is about balancing speed with reliabiliy to your keep pipelines green, ultimately ensuring the stability of collaboration across branches is sustained. GitLab has introduced Merge Trains as an important way to accomplish this. When merge trains are used, each merge request joins as the last item in that train. Each merge request is processed in order and is added to the merge result of the last successful merge request. The merge request adds its changes, and starts the pipeline immediately under the assumption that everything is going to pass. If the merge request fails, it is kicked out of the train and the next merge request continues the pipeline of the last successful merge result. 

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AMerge%20Trains)
- [Overall Vision](/direction/ops/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/) 

## What's Next & Why

For an overview of our plans for Merge Trains, see epic [Merge Trains Vision](https://gitlab.com/groups/gitlab-org/-/epics/5122).

In the first two quarters of FY22, we are focused on reducing the [bug backlog of the Merge Train feature](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=Category%3AMerge%20Trains&label_name[]=bug). Our goal is to improve usability by resolving issues such as conditions that prevent a user from re-adding a merge request to Merge Train when "pipeline must succeed" is enabled [gitlab#35135](https://gitlab.com/gitlab-org/gitlab/-/issues/35135). 

In the latter half of FY22, we plan to proceed with other Merge Trains vision items such as [visualization of a merge train in a merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/277391) and [fast-forward merge support for merge trains](https://gitlab.com/groups/gitlab-org/-/epics/4911).

### Overview

<!-- blank line -->
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/H2KPKDBW6Tw" frameborder="0" allowfullscreen></iframe>
</figure>
<!-- blank line -->


## Who we are focusing on? 

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Merge Trains, our "What's Next & Why" are targeting the following personas, as ranked by priority for support: 

1. [Sasha - Software Developer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. [Devon - DevOps Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
 
## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is Complete ([see our definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).
We currently have basic capabilities and want to continue and extend these in the upcoming releases.

Key deliverables to achieve this are:

- [Run pipeline on merge result](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380) (Complete)
- [Parallel execution strategy for Merge Trains](https://gitlab.com/gitlab-org/gitlab-ee/issues/11222) (Complete)

- [Merge Trains should support fast forward merge](https://gitlab.com/gitlab-org/gitlab/issues/35628)
- [API support for merge trains](https://gitlab.com/gitlab-org/gitlab/issues/32665)
- [Add Merge Train support to AutoDevOps](https://gitlab.com/gitlab-org/gitlab/issues/121933)
- [Add/Remove TODO tasks according to the Merge Train events](https://gitlab.com/gitlab-org/gitlab/issues/12136) (Complete)
- [Have merge quick action add to merge train](https://gitlab.com/gitlab-org/gitlab/issues/32336) (Complete)
- [Support Merge Train in Git Push Option](https://gitlab.com/gitlab-org/gitlab/issues/32732)

## Competitive Landscape

It looks like GitLab is the first to provide this functionality, but GitHub has a [partial solution](https://discourse.drone.io/t/github-claims-that-merge-refs-are-undocumented-feature/1100) and we are aware that other competitors 
will be able to catch up. We will be monitoring this space closely. 

## Top Customer Issue(s) and Top Customer Success/Sales Issue(s)

The most popular issue is [gitlab#35628](https://gitlab.com/gitlab-org/gitlab/issues/35628). When selecting to work with Fast Forward Merge without 
using merge trains, a user is offered to rebase master manually, in case master has advanced.
Merge Train supporting fast forward merge would reconstruct train and re-run pipelines automatically.

## Top Internal Customer Issue(s)

One very useful functionality that has been requested by our own team while dogfooding the feature, is the ability to use merge quick action to 
add an MR to a merge train [gitlab#32336](https://gitlab.com/gitlab-org/gitlab/issues/32336).

### Delivery Team
 
In an effort to dogfood our own features, we are actively working on using merge trains internally on gitlab-org [gitlab#195](https://gitlab.com/gitlab-org/quality/team-tasks/issues/195).

## Top Vision Item(s)

Our top vision item is [Merge Trains should support fast forward merge](https://gitlab.com/gitlab-org/gitlab/issues/35628) as this will eliminate the
frequent need for manually rebasing. We have heard from many developers that a large portion of their day is dedicated to this activity, and with 
this functionality, Merge trains will take care of this for them. 

